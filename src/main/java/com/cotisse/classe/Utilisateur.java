/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;

/**
 *
 * @author hp
 */
public class Utilisateur {
    
    String id;
    String nom;
    String numero;
    Date dateDeNaissance;
    String motDePasse;
    String sexe;
    String profil;

    public Utilisateur() {
    }

    public Utilisateur(String id, String nom, String numero, Date dateDeNaissance, String motDePasse, String sexe, String profil) {
        this.id = id;
        this.nom = nom;
        this.numero = numero;
        this.dateDeNaissance = dateDeNaissance;
        this.motDePasse = motDePasse;
        this.sexe = sexe;
        this.profil = profil;
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getNumero() {
        return numero;
    }

    public Date getDateDeNaissance() {
        return dateDeNaissance;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public String getSexe() {
        return sexe;
    }

    public String getProfil() {
        return profil;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setDateDeNaissance(Date dateDeNaissance) {
        this.dateDeNaissance = dateDeNaissance;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
        
}
