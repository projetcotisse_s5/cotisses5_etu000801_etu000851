/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Vehicule {
    
    String id;
    String modele;
    int nbPlace;
    String categorie;

    public Vehicule() {
    }

    public Vehicule(String id, String modele, int nbPlace, String categorie) {
        this.id = id;
        this.modele = modele;
        this.nbPlace = nbPlace;
        this.categorie = categorie;
    }

    public String getId() {
        return id;
    }

    public String getModele() {
        return modele;
    }

    public int getNbPlace() {
        return nbPlace;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }
        
}
