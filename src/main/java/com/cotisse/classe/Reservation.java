/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;

/**
 *
 * @author hp
 */
public class Reservation {
    
    String id;
    String utilisateur;
    String voyage;
    int nbPlace;
    double prix;
    int etat;
    Date date;
    Date delai;

    public Reservation() {
    }

    public Reservation(String id, String utilisateur, String voyage, int nbPlace, double prix, int etat, Date date, Date delai) {
        this.id = id;
        this.utilisateur = utilisateur;
        this.voyage = voyage;
        this.nbPlace = nbPlace;
        this.prix = prix;
        this.etat = etat;
        this.date = date;
        this.delai = delai;
    }

    public String getId() {
        return id;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public String getVoyage() {
        return voyage;
    }

    public int getNbPlace() {
        return nbPlace;
    }

    public double getPrix() {
        return prix;
    }

    public int getEtat() {
        return etat;
    }

    public Date getDate() {
        return date;
    }

    public Date getDelai() {
        return delai;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    public void setVoyage(String voyage) {
        this.voyage = voyage;
    }

    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDelai(Date delai) {
        this.delai = delai;
    }
        
}
