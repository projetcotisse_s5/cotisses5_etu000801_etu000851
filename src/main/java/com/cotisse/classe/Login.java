/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Login {
    
    String numero;
    String motDePasse;

    public Login() {
    }

    public Login(String numero, String motDePasse) {
        this.numero = numero;
        this.motDePasse = motDePasse;
    }

    public String getNumero() {
        return numero;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
        
}
