/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Compte {
    
    String id;
    String utilisateur;
    double montant;

    public Compte() {
    }

    public Compte(String id, String utilisateur, double montant) {
        this.id = id;
        this.utilisateur = utilisateur;
        this.montant = montant;
    }

    public String getId() {
        return id;
    }

    public String getUtilisateur() {
        return utilisateur;
    }

    public double getMontant() {
        return montant;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setUtilisateur(String utilisateur) {
        this.utilisateur = utilisateur;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
        
}
