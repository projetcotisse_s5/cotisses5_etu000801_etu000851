/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Categorie {
    
    String id;
    String designation;
    int pourcentage;

    public Categorie() {
    }

    public Categorie(String id, String designation, int pourcentage) {
        this.id = id;
        this.designation = designation;
        this.pourcentage = pourcentage;
    }

    public String getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public int getPourcentage() {
        return pourcentage;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setPourcentage(int pourcentage) {
        this.pourcentage = pourcentage;
    }
            
}
