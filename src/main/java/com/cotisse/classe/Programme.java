/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author hp
 */
public class Programme {
    
    String id;
    Date date;
    String depart;
    String arrivee;
    Timestamp heureDepart;
    Timestamp heureArrivee;
    int distance;

    public Programme() {
    }

    public Programme(String id, Date date, String depart, String arrivee, Timestamp heureDepart, Timestamp heureArrivee, int distance) {
        this.id = id;
        this.date = date;
        this.depart = depart;
        this.arrivee = arrivee;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public Timestamp getHeureDepart() {
        return heureDepart;
    }

    public Timestamp getHeureArrivee() {
        return heureArrivee;
    }

    public int getDistance() {
        return distance;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public void setHeureDepart(Timestamp heureDepart) {
        this.heureDepart = heureDepart;
    }

    public void setHeureArrivee(Timestamp heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }
    
}
