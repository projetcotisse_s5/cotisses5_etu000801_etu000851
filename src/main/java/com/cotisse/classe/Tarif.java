/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Tarif {
    
    String id;
    String depart;
    String arrivee;
    double prix;

    public Tarif() {
    }

    public Tarif(String id, String depart, String arrivee, double prix) {
        this.id = id;
        this.depart = depart;
        this.arrivee = arrivee;
        this.prix = prix;
    }

    public String getId() {
        return id;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public double getPrix() {
        return prix;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
        
}
