/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;

/**
 *
 * @author hp
 */
public class SearchV {
    
    String depart;
    String arrivee;
    Date date;

    public SearchV() {
    }

    public SearchV(String depart, String arrivee, Date date) {
        this.depart = depart;
        this.arrivee = arrivee;
        this.date = date;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public Date getDate() {
        return date;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
}
