/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hp
 */

@XmlRootElement
public class Place {
    
    String id;
    int numero;
    int etat;
    String vehicule;    

    public Place() {
    }

    public Place(String id, int numero, int etat, String vehicule) {
        this.id = id;
        this.numero = numero;
        this.etat = etat;
        this.vehicule = vehicule;
    }

    public String getId() {
        return id;
    }

    public int getNumero() {
        return numero;
    }

    public int getEtat() {
        return etat;
    }

    public String getVehicule() {
        return vehicule;
    }        
    
    public void setId(String id) {
        this.id = id;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setEtat(int etat) {
        this.etat = etat;
    }

    public void setVehicule(String vehicule) {
        this.vehicule = vehicule;
    }
           
}
