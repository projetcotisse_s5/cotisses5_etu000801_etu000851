/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;
import java.sql.Timestamp;

/**
 *
 * @author hp
 */
public class ListeTrajet {
    
    String id;
    String depart;
    String arrivee;
    Date date;
    Timestamp heureDepart;
    Timestamp heureArrivee;
    int distance;
    String modele;
    int nbPlace;
    String categorie;
    int pourcentage;    

    public ListeTrajet() {
    }

    public ListeTrajet(String id, String depart, String arrivee, Date date, Timestamp heureDepart, Timestamp heureArrivee, int distance, String modele, int nbPlace, String categorie, int pourcentage) {
        this.id = id;
        this.depart = depart;
        this.arrivee = arrivee;
        this.date = date;
        this.heureDepart = heureDepart;
        this.heureArrivee = heureArrivee;
        this.distance = distance;
        this.modele = modele;
        this.nbPlace = nbPlace;
        this.categorie = categorie;
        this.pourcentage = pourcentage;
    }

    public String getId() {
        return id;
    }

    public String getDepart() {
        return depart;
    }

    public String getArrivee() {
        return arrivee;
    }

    public Date getDate() {
        return date;
    }

    public Timestamp getHeureDepart() {
        return heureDepart;
    }

    public Timestamp getHeureArrivee() {
        return heureArrivee;
    }

    public int getDistance() {
        return distance;
    }

    public String getModele() {
        return modele;
    }

    public int getNbPlace() {
        return nbPlace;
    }

    public String getCategorie() {
        return categorie;
    }

    public int getPourcentage() {
        return pourcentage;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public void setArrivee(String arrivee) {
        this.arrivee = arrivee;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setHeureDepart(Timestamp heureDepart) {
        this.heureDepart = heureDepart;
    }

    public void setHeureArrivee(Timestamp heureArrivee) {
        this.heureArrivee = heureArrivee;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setNbPlace(int nbPlace) {
        this.nbPlace = nbPlace;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public void setPourcentage(int pourcentage) {
        this.pourcentage = pourcentage;
    }
  
    
}
