/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Voyage {
    
    String id;
    String programme;
    String vehicule;

    public Voyage() {
    }

    public Voyage(String id, String programme, String vehicule) {
        this.id = id;
        this.programme = programme;
        this.vehicule = vehicule;
    }

    public String getId() {
        return id;
    }

    public String getProgramme() {
        return programme;
    }

    public String getVehicule() {
        return vehicule;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setProgramme(String programme) {
        this.programme = programme;
    }

    public void setVehicule(String vehicule) {
        this.vehicule = vehicule;
    }
        
}
