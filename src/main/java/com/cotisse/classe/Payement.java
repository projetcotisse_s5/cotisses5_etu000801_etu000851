/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

import java.sql.Date;

/**
 *
 * @author hp
 */
public class Payement {
    
    String id;
    String reservation;
    Date date;
    double montant;

    public Payement() {
    }

    public Payement(String id, String reservation, Date date, double montant) {
        this.id = id;
        this.reservation = reservation;
        this.date = date;
        this.montant = montant;
    }

    public String getId() {
        return id;
    }

    public String getReservation() {
        return reservation;
    }

    public Date getDate() {
        return date;
    }

    public double getMontant() {
        return montant;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setReservation(String reservation) {
        this.reservation = reservation;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }
        
}
