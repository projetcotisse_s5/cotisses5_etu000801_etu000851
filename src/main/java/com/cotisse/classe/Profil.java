/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.classe;

/**
 *
 * @author hp
 */
public class Profil {
    
    String id;
    String designation;

    public Profil(String id, String designation) {
        this.id = id;
        this.designation = designation;
    }

    public Profil() {
    }

    public String getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }
    
    
}
