/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.connection;

/**
 *
 * @author hp
 */

import java.sql.*;

public class DbConnection {
    Connection co;
    Statement stmt;
    ResultSet rs;
    public Connection getConnection(){
            return this.co;
    }
    public void clear()throws Exception{
            co.close();
            stmt.close();
    }
    public Statement getStatement(){
            return this.stmt;
    }
    public DbConnection(){

        try{
            Class.forName("org.postgresql.Driver");
            this.co = DriverManager
               .getConnection("jdbc:postgresql://localhost:5432/cotisse",
               "postgres", "0000");
            this.stmt = this.co.createStatement();
            System.out.println("connecte !");
        }catch(Exception e){

        }	
    }
    public void setAutoCommit(boolean value)throws Exception{
            this.co.setAutoCommit(value);
    }
    public void commit()throws Exception{
            this.co.commit();
    }
    public void rollback()throws Exception{
            this.co.rollback();
    }
}
