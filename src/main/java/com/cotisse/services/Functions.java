/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.services;

import com.cotisse.classe.*;
import com.cotisse.classe.Utilisateur;
import java.sql.Connection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hp
 */
public class Functions {
    
    public Utilisateur login(String numero,String motDePasse,Connection con)throws Exception{        
        Reflects reflects = new Reflects();        
        List<Object> o = reflects.selectWithCondition("utilisateur",new Utilisateur(),"where numero='"+numero+"' and motDePasse=md5('"+motDePasse+"')",con);
        if(o.size()==0) {
            throw new Exception("Numero ou mot de passe incorrect!!");
        }
        return (Utilisateur)o.get(0);                      
    }
    
    public void register(Utilisateur user,Connection con)throws Exception{
        try{
            Reflects reflects = new Reflects();
            reflects.insert("utilisateur",user,con);
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public Lieu[] getLieu(Connection con)throws Exception{
        try{
            
            Reflects reflects = new Reflects();
            List<Object> lieu = reflects.select("lieu",new Lieu(),con);
            Lieu[] result = new Lieu[lieu.size()];
            result = lieu.toArray(result);
            return result;
        }
        catch(Exception e){
            throw e;
        }
    }
    
    public Tarif getTarif(String depart,String arrivee,Connection con)throws Exception{
        try{
            Reflects reflects = new Reflects();
            List<Object> o = reflects.selectWithCondition("Tarif",new Tarif(),"where depart='"+depart+"' and arrivee='"+arrivee+"'",con);
            Tarif[] result = new Tarif[o.size()];
            result = o.toArray(result);
            return result[0];
        }
        catch(Exception e){
            Exception message = new Exception("Error 404 Not Found: Tarif existant");
            throw message;
        }
    }
       
    public ListeTrajet[] getListeTrajetSearch(String depart, String arrivee, Date date, Connection con)throws Exception{
        try{
            Reflects reflects = new Reflects();
            List<Object> o = reflects.selectWithCondition("ListeTrajet",new ListeTrajet(),"where depart='"+depart+"' and arrivee='"+arrivee+"' and date='"+date+"'",con);
            ListeTrajet[]  result = new ListeTrajet[o.size()];
            result = o.toArray(result);
            return result;
        }
        catch(Exception e){
            Exception message = new Exception("Error 404 Not Found: Aucun trajet correspondant");
            throw message;
        }
    }
    
    public SearchV valideDA(SearchV s) throws Exception{
        try{
            SearchV result  = new SearchV();
            if(s.getDepart()==s.getArrivee()){
                throw new Exception("Erreur 701: La destination est la meme que l'arrivee");
            }            
            else{
             result = s;
             return result;
            }
        }
        catch(Exception e){            
            throw e;
        }
    }
    
    public ListeTrajetDetail[] getListeDetails(ListeTrajet[] liste, double prix)throws Exception{
        try{
            List<ListeTrajetDetail> res = new ArrayList<ListeTrajetDetail>();            
            for(int i = 0; i<liste.length; i++){
                double vraiPrix = prix + (liste[i].getPourcentage()*prix);
                ListeTrajetDetail temp = new ListeTrajetDetail(liste[i].getId(),liste[i].getDepart(),liste[i].getArrivee(),liste[i].getDate(),liste[i].getHeureDepart(),liste[i].getHeureArrivee(),liste[i].getDistance(),liste[i].getModele(),liste[i].getNbPlace(),liste[i].getCategorie(),liste[i].getPourcentage(),vraiPrix);
                res.add(temp);
            }
            ListeTrajetDetail[] result = new ListeTrajetDetail[res.size()];
            result = res.toArray(result);
            return result;
        }
        catch(Exception e){
            Exception message = new Exception("Error: Cannot get liste detail");
            throw message;
        }
    }
}
