/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.services;

/**
 *
 * @author hp
 */

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Utils {
    
    /**
 * Method Processing
 */

    public static Field getSpecificField(Object object, String fieldName)throws Exception{
        try {
            return object.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            boolean isFound=false;
            Field[] fields=object.getClass().getDeclaredFields();
            for (Field field : fields) {
                if(field.getName().compareToIgnoreCase(fieldName)==0){
                    isFound=true;
                    return field;
                }
            }
            if(!isFound){
                throw new Exception("The Field "+fieldName+" is not found for the Class "+object.getClass().getSimpleName());
            }
            return null;
        }
    }
    public static Method getGettersOf(Object object,String fieldName)throws Exception{
        try {
            return object.getClass().getDeclaredMethod("get".concat(fieldName.substring(0,1).toUpperCase().concat(fieldName.substring(1,fieldName.length()))));
        } catch (NoSuchMethodException e) {
            Field field=Utils.getSpecificField(object, fieldName);
            return object.getClass().getDeclaredMethod("get".concat(field.getName().substring(0,1).toUpperCase().concat(field.getName().substring(1,field.getName().length()))));
        }
        catch(Exception e){
            throw new Exception("set"+fieldName+" is not Found for the Class "+object.getClass().getSimpleName());
        }
    }
    public static Method getSettersOf(Object object, String fieldName) throws Exception{
        try {
            Field field=Utils.getSpecificField(object, fieldName);
            return object.getClass().getMethod("set".concat(field.getName().substring(0,1).toUpperCase().concat(field.getName().substring(1,field.getName().length()))),field.getType());
        }
        catch(Exception e){
            throw new Exception("set"+fieldName+" is not Found for the Class "+object.getClass().getSimpleName());
        }
    }


/**
 * Digit Processing
 */
    public static String[] splitNumberToHundreds(double number) {
        String numberString=String.valueOf((int)number);
        int numberLength=numberString.length();
        int resultLength=(numberLength-numberLength%3)/3;
        if(numberLength%3!=0){
            ++resultLength;
        }
        String[] result=new String[resultLength];
        int indice=resultLength-1;
            String temp="";
            int count=0;
            for(int j=numberString.length()-1;j>=0;j--){
                ++count;
                temp=String.valueOf(numberString.charAt(j)).concat(temp);
                if((count%3==0 && count != 0) || j==0){
                    result[indice--]=temp;
                    temp="";
                }
            }
        return result;
    }
    public static String hundredToString(int number){
        String result = "";
        String[] unity1 = {"","un","deux","trois","quatre","cinq","six","sept","huit","neuf"};
        String[] unity2 = {"","onze","douze","treize","quatorze","quinze","seize","dix-sept","dix-huit","dix-neuf"};
        String[] decade = {"","dix","vingt","trente","quarante","cinquante","soixante","soixante","quatre-vingt","quatre-vingt"};
        int[] divideNumber = new int[3];
        int puissance = 2, numberTemp = number, puissanceCalcul = 0;
            for (int i = 0; i < divideNumber.length; i++) {
                puissanceCalcul = (int)Math.pow(10,puissance);
                divideNumber[i] =(int)numberTemp/puissanceCalcul;
                numberTemp =(int) numberTemp-(divideNumber[i]*puissanceCalcul);
                puissance--;
            }
            if(divideNumber[0] != 0){
                if(divideNumber[0] != 1)
                {
                    result +=unity1[divideNumber[0]]+" cent";
                }else{
                    result += "cent";
                }
                
            }
            if(divideNumber[1] != 0){
                if(divideNumber[1] != 1){
                    result +=" "+decade[divideNumber[1]];
                    if(divideNumber[2] != 0)
                    {
                        if(divideNumber[1] == 7 || divideNumber[1] == 9)
                        {
                            result +=" "+unity2[divideNumber[2]];
                        }else{
                            result +=" "+unity1[divideNumber[2]];
                        }
                    }
                }else{
                    result +=" "+unity2[divideNumber[2]];
                }

            }else{
                result += unity1[divideNumber[2]];
            }
        
        return result;
    }
    public static List<Double> splitNumber(double number) {
        List<Double> result=new ArrayList<>();
        int entier = new Double(number).intValue(); 
        double decimale = number-(new Double(entier).doubleValue());
        result.add(Double.valueOf(String.valueOf(entier)));
        if(decimale>0){
            
            String numberString=String.valueOf(number);
            int index=numberString.indexOf(".");
            result.add(Double.valueOf(numberString.substring(index+1,numberString.length())));
        }
        return result;
    }
    public static String letterNumber(double number){
        String result = "";
        String[] plusHundred = {"","mille","million","milliard"};
        String[] hundredSplit = Utils.splitNumberToHundreds(number);
        int[] hundredSplitInt = new int[hundredSplit.length];
        for (int i = 0; i <hundredSplit.length; i++) {
            hundredSplitInt[i] = Integer.parseInt(hundredSplit[i]);
        }
        String[] resultTemp = new String[hundredSplit.length];
        for (int i = 0; i < resultTemp.length; i++) {
            resultTemp[i] = Utils.hundredToString(hundredSplitInt[i]);
        }
        int indice = resultTemp.length - 1;
        String hundredTemp = "";
        for (int i = 0; i < resultTemp.length; i++) {
            if(hundredSplitInt[i]==1 || indice == 0){
                hundredTemp = plusHundred[indice];
            }else{
                hundredTemp = plusHundred[indice]+"s";
            }
            if(hundredSplitInt[i]==1 && indice == 1){
                result +=" "+hundredTemp;
                indice--;
            }else{
                result +=" "+resultTemp[i]+" "+hundredTemp;
                indice--;
            }
        }
        return result;
    }
    public static String convertToLetter(double number){
        List<Double> listeNumber=Utils.splitNumber(number);
        String result=Utils.letterNumber(listeNumber.get(0));
        if(listeNumber.size()>1){
            result+=" virgule ";
            result+=Utils.letterNumber(listeNumber.get(1));
        }
        return result;
    }
    public static String hashNumber(double number){
        List<Double> listeNumber=Utils.splitNumber(number);
        String result="";
        for (int i = 0; i < listeNumber.size(); i++) {
            String temp="";
            String[] table=Utils.splitNumberToHundreds(listeNumber.get(i));
            for(int j=0;j<table.length;j++){
                temp+=table[j]+" ";
            }
            result+=temp;
                if(i==0){
                    result+=",";
                }
        }
        return result;
    }

/**
 * Date Processing
 */
    public static Date convertToDate(String dateString)throws Exception{
    	dateString=dateString.trim();
        Date date=null;
        Pattern dateFormat=Pattern.compile("[0-9]{4}.[0-1][1-9].[0-9]{2}");
        Matcher matchFormat=dateFormat.matcher(dateString);
        Pattern splitter=Pattern.compile("\\W");
        if(matchFormat.matches()){
            String[] tableauDate=splitter.split(dateString,3);
            dateString=""+tableauDate[0]+"-"+tableauDate[1]+"-"+tableauDate[2]+"";
            date=Date.valueOf(dateString);
        }
        else{
            dateFormat=Pattern.compile("[0-9]{2}.[0-1][1-9].[0-9]{4}");
            matchFormat=dateFormat.matcher(dateString);
            if(matchFormat.matches()){
                String[] tableauDate=splitter.split(dateString,3);
                dateString=""+tableauDate[2]+"-"+tableauDate[1]+"-"+tableauDate[0]+"";
                date=Date.valueOf(dateString);
            }
            else{
                throw new Exception("Invalid Date Format");
            }
        }
        return date;
    }
    public static int between2DateInDays(Date dateFirst,Date dateSecond) {
        if(dateFirst.compareTo(dateSecond)>0){
            Date temp=dateSecond;
            dateSecond=dateFirst;
            dateFirst=temp;
        }
        long difference=dateSecond.getTime()-dateFirst.getTime();
        float result=(difference / (1000*60*60*24));
        return (int) result;
        
    }

/**
 * Data Structure Processing
 */
    public static void sortByDescending(List<Object> objects,String fieldName) throws Exception{
        if(objects.size()==0){
            throw new Exception("Sort Invalid : Object list must be initialize");
        }
        Field field=Utils.getSpecificField(objects.get(0), fieldName);
        Method method=Utils.getGettersOf(objects.get(0), fieldName);
        for (int i = 0; i < objects.size(); i++) {
            for(int j=0;j<objects.size()-1;j++){
                String fieldTypeName=field.getType().getSimpleName();
                if(fieldTypeName.compareToIgnoreCase("Double")==0){
                    if((double)method.invoke(objects.get(j))<(double)method.invoke(objects.get(j+1))){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("int")==0 || fieldTypeName.compareToIgnoreCase("Integer")==0){
                    if((int)method.invoke(objects.get(j))<(int)method.invoke(objects.get(j+1))){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("Timestamp")==0){
                    if(((Timestamp)method.invoke(objects.get(j))).compareTo((Timestamp)method.invoke(objects.get(j+1)))==-1){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("Date")==0){
                    if(((Date)method.invoke(objects.get(j))).compareTo((Date)method.invoke(objects.get(j+1)))==-1){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
            }
        }
    }
    public static void sortByIncreasing(List<Object>objects,String fieldName)throws Exception{
        if(objects.size()==0){
            throw new Exception("Sort Invalid : Object list must be initialize");
        }
        Field field=Utils.getSpecificField(objects.get(0), fieldName);
        Method method=Utils.getGettersOf(objects.get(0), fieldName);
        for (int i = 0; i < objects.size(); i++) {
            for(int j=0;j<objects.size()-1;j++){
                String fieldTypeName=field.getType().getSimpleName();
                if(fieldTypeName.compareToIgnoreCase("Double")==0){
                    if((double)method.invoke(objects.get(j))>(double)method.invoke(objects.get(j+1))){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("int")==0 || fieldTypeName.compareToIgnoreCase("Integer")==0){
                    if((int)method.invoke(objects.get(j))>(int)method.invoke(objects.get(j+1))){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("Timestamp")==0){
                    if(((Timestamp)method.invoke(objects.get(j))).compareTo((Timestamp)method.invoke(objects.get(j+1)))==1){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
                else if(fieldTypeName.compareToIgnoreCase("Date")==0){
                    if(((Date)method.invoke(objects.get(j))).compareTo((Date)method.invoke(objects.get(j+1)))==1){
                        Object temp=objects.get(j+1);
                        objects.set(j+1,objects.get(j));
                        objects.set(j,temp);
                    }  
                }
            }
        }
    }
    public static void sortByDescending(List<Object> objects,String[] fieldsName)throws Exception{
        for (String fieldName : fieldsName) {
            Utils.sortByDescending(objects, fieldName);
        }
    }
    public static void sortByIncreasing(List<Object> objects,String[] fieldsName)throws Exception{
        for (String fieldName : fieldsName) {
            Utils.sortByIncreasing(objects, fieldName);
        }
    }

/**
 * Equivalent Database Processing
 */
    public static List<Object> find(List<Object> objects,String fieldName,String operation,String condition)throws Exception{
        if(objects.size()==0){
            throw new Exception("Find Invalid : Object list must be initialize");
        }
        List<Object> result=new ArrayList<>();
        String fieldTypeName= Utils.getSpecificField(objects.get(0), fieldName).getType().getSimpleName();
        Method method=Utils.getGettersOf(objects.get(0), fieldName);
        if(fieldTypeName.compareToIgnoreCase("Double")==0){
            double doubleCondition=Double.valueOf(condition);
            for (Object object : objects) {
                if(operation.compareToIgnoreCase("==")==0 || operation.compareToIgnoreCase("like") ==0 || operation.compareToIgnoreCase("<=")==0 || operation.compareToIgnoreCase(">=")==0){
                    if((double)method.invoke(object)==doubleCondition){
                        result.add(object);
                        objects.remove(object);
                    }               
                }
                else if(operation.compareToIgnoreCase("<")==0){
                    if((double)method.invoke(object)<doubleCondition){
                        result.add(object);
                        objects.remove(object);
                    }  
                }
                else if(operation.compareToIgnoreCase(">")==0){
                    if((double)method.invoke(object)>doubleCondition){
                        result.add(object);
                        objects.remove(object);
                    }  
                }
            }
        }
        if(fieldTypeName.compareToIgnoreCase("Integer")==0|| fieldTypeName.compareToIgnoreCase("int")==0){
            int intCondition=Integer.valueOf(condition);
            for (Object object : objects) {
                if(operation.compareToIgnoreCase("==")==0 || operation.compareToIgnoreCase("like") ==0 || operation.compareToIgnoreCase("<=")==0 || operation.compareToIgnoreCase(">=")==0){
                    if((int)method.invoke(object)==intCondition){
                        result.add(object);
                        objects.remove(object);
                    }               
                }
                else if(operation.compareToIgnoreCase("<")==0){
                    if((int)method.invoke(object)<intCondition){
                        result.add(object);
                        objects.remove(object);
                    }  
                }
                else if(operation.compareToIgnoreCase(">")==0){
                    if((int)method.invoke(object)>intCondition){
                        result.add(object);
                        objects.remove(object);
                    }  
                }
            }
        }
        if(fieldTypeName.compareToIgnoreCase("Date")==0){
            Date dateCondition=Date.valueOf(condition);
            for (Object object : objects) {
                if(operation.compareToIgnoreCase("==")==0 || operation.compareToIgnoreCase("like") ==0 || operation.compareToIgnoreCase("<=")==0 || operation.compareToIgnoreCase(">=")==0){
                    if(((Date)method.invoke(object)).compareTo(dateCondition)==0){
                        result.add(object);
                        objects.remove(object);
                    }               
                }
                else if(operation.compareToIgnoreCase("<")==-1){
                    if(((Date)method.invoke(object)).compareTo(dateCondition)==0){
                        result.add(object);
                        objects.remove(object);
                    }
                }
                else if(operation.compareToIgnoreCase(">")==1){
                    if(((Date)method.invoke(object)).compareTo(dateCondition)==0){
                    result.add(object);
                    objects.remove(object);
                } 
                }
            }
        }
        if(fieldTypeName.compareToIgnoreCase("Timestamp")==0){
            Timestamp timestampCondition=Timestamp.valueOf(condition);
            for (Object object : objects) {
                if(operation.compareToIgnoreCase("==")==0 || operation.compareToIgnoreCase("like") ==0 || operation.compareToIgnoreCase("<=")==0 || operation.compareToIgnoreCase(">=")==0){
                    if(((Timestamp)method.invoke(object)).compareTo(timestampCondition)==0){
                        result.add(object);
                        objects.remove(object);
                    }               
                }
                else if(operation.compareToIgnoreCase("<")==-1){
                    if(((Timestamp)method.invoke(object)).compareTo(timestampCondition)==0){
                        result.add(object);
                        objects.remove(object);
                    }
                }
                else if(operation.compareToIgnoreCase(">")==1){
                    if(((Timestamp)method.invoke(object)).compareTo(timestampCondition)==0){
                    result.add(object);
                    objects.remove(object);
                } 
                }
            }
        }
        return result;
    }  
    
}
