/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cotisse.cotisse;

/**
 *
 * @author hp
 */

import com.cotisse.classe.*;
import com.cotisse.connection.DbConnection;
import com.cotisse.services.Functions;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/main")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Main {
    
    DbConnection db = new DbConnection();
    
    @GET
    @Path("/")
    
    public Response welcome() {
        String message = "Welcome to Cotisse Web service.";                
        return Response.status(200).entity(message).build();
    }
    
    @POST    
    @Path("/login")    
    public Response login(Login login) throws Exception {

            Functions f = new Functions();            
            try {
                    Utilisateur users = f.login(login.getNumero(), login.getMotDePasse(), db.getConnection());
                    return Response.status(200)
                    .entity(users)
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                    .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
                    .allow("OPTIONS").build();
            }
            catch(Exception e)
            {
                    return Response.status(404)
                    .entity(e.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                    .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
                    .allow("OPTIONS").build();
            }            
    }
    
    @POST
    @Path("/register")
    public Response register(Utilisateur user) throws Exception{
        
        Functions f = new Functions();
        try{
            Utilisateur result = new Utilisateur(user.getId(),user.getNom(),user.getNumero(),user.getDateDeNaissance(),user.getMotDePasse(),user.getSexe(),user.getProfil());
            f.register(result, db.getConnection());
            return Response.status(200)
            .entity("Success")
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
            .allow("OPTIONS").build();
        }
        catch(Exception e){
            return Response.status(404)
            .entity(e.toString())
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
            .allow("OPTIONS").build();
        }
    }
    
    @GET    
    @Path("/lieu")    
    public Response getLieu() throws Exception {

            Functions f = new Functions();            
            try {
                    Lieu[] lieu = f.getLieu(db.getConnection());
                    return Response.status(200)
                    .entity(new GenericEntity<Lieu[]>(lieu){})
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                    .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
                    .allow("OPTIONS").build();
            }
            catch(Exception e)
            {
                    return Response.status(404)
                    .entity(e.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                    .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
                    .allow("OPTIONS").build();
            }            
    }
    
    @POST
    @Path("/recherche")
    public Response search(SearchV searchV)throws Exception{
        
        Functions f = new Functions();
        try{
            SearchV valide = f.valideDA(searchV);
            Tarif tarif = f.getTarif(valide.getDepart(), valide.getArrivee(), db.getConnection());
            double prixDeBase = tarif.getPrix();
            ListeTrajet[] liste = f.getListeTrajetSearch(valide.getDepart(), valide.getArrivee(), valide.getDate(), db.getConnection());
            ListeTrajetDetail[] result = f.getListeDetails(liste,prixDeBase);
            return Response.status(200)
            .entity(new GenericEntity<ListeTrajet[]>(liste){})
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }
        catch(Exception e){
            return Response.status(404)  
            .entity(e.toString())
            .header("Access-Control-Allow-Origin", "*")
            .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
            .header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
            .allow("OPTIONS").build();
        }            
    }
}
